using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHandler : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "ground")
        {
            Destroy(gameObject);
        }
        else if (collision.collider.tag == "Ice" && transform.tag == "Bomb" && !GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Bomb>().Blast();
        }
    }
}
