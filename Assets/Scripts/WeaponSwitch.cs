using System;
using UnityEngine;

public class WeaponSwitch : MonoBehaviour
{
    public enum ProjectileName
    {
        Ball,
        Knife,
        Bomb
    }
    public Transform ball, knife, bomb;
    public AimAndShoot aimAndShoot;

    public void SelectWeapon(int projectileNumber)
    {
        aimAndShoot.Projectile = (ProjectileName)projectileNumber switch
        {
            ProjectileName.Ball => ball,
            ProjectileName.Knife => knife,
            ProjectileName.Bomb => bomb,
            _ => throw new ArgumentOutOfRangeException(nameof(projectileNumber), projectileNumber, null)
        };
    }
}
