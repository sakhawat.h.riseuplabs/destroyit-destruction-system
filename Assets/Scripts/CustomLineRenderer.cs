using UnityEngine;
using UnityEngine.UI;

public class CustomLineRenderer : MonoBehaviour
{
    Vector2 startPoint = Vector2.zero;
    Vector2 endPoint = Vector2.zero;

    private Image image;
    private RectTransform rectTransform;

    void Start()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
    }

    public CustomLineRenderer SetPointPosition(int order, Vector2 position)
    {
        if (order == 0)
            this.startPoint = position;
        else
            this.endPoint = position;
        return this;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            startPoint = transform.parent.InverseTransformPoint(startPoint);
            endPoint = transform.parent.InverseTransformPoint(endPoint);

            Vector3 dif = endPoint - startPoint;

            rectTransform.localPosition = (startPoint + endPoint) / 2;
            rectTransform.sizeDelta = new Vector3(50, dif.magnitude);
            rectTransform.localRotation = Quaternion.FromToRotation(Vector3.up, dif);
        }
    }
}
