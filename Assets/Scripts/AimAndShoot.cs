using System.Collections.Generic;
using RayFire;
using UnityEngine;
using UnityEngine.EventSystems;

public class AimAndShoot : MonoBehaviour
{
    bool isDrag = false;
    public float speed = 5.0f;
    public Transform Projectile;
    private Transform currentProjectile;
    public Transform dummyKnifeProjectile;
    public Transform spawnObject;

    float distance = 0;
    public LineRenderer lineRenderer;
    public CustomLineRenderer customlineRenderer;

    public List<GameObject> allProjectiles = new List<GameObject>();

    public Transform imgProjection;

    private void Start()
    {
        Application.targetFrameRate = 60;
        //lineRenderer.positionCount = 2;
        dummyKnifeProjectile = Instantiate(dummyKnifeProjectile, spawnObject.position, Quaternion.identity);
        dummyKnifeProjectile.gameObject.SetActive(false);
    }

    private void Update()
    {
        bool isPointOnButton = EventSystem.current.currentSelectedGameObject is not null;

        if (Input.GetMouseButtonDown(0) && !isPointOnButton)
        {
            isDrag = true;
            currentProjectile = Instantiate(Projectile, spawnObject.position, Quaternion.identity);
            if (currentProjectile.tag == "Knife")
            {
                dummyKnifeProjectile.gameObject.SetActive(true);
                dummyKnifeProjectile.rotation = Quaternion.Euler(0, 0, -180);
                //currentProjectile.gameObject.SetActive(false);
            }
            //imgProjection.gameObject.SetActive(true);
            imgProjection.position = Camera.main.WorldToScreenPoint(spawnObject.position);

            //customlineRenderer.SetPointPosition(0, imgProjection.position);
            //customlineRenderer.SetPointPosition(1, Input.mousePosition);
            customlineRenderer.gameObject.SetActive(true);
        }

        if (Input.GetMouseButton(0) && !isPointOnButton)
        {
            if (!isDrag)
                return;

            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));

            Vector2 direction = Input.mousePosition - imgProjection.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
            //Debug.Log(rotation);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);
            if (currentProjectile.tag == "Knife")
            {
                dummyKnifeProjectile.rotation = Quaternion.Slerp(dummyKnifeProjectile.rotation, rotation, speed * Time.deltaTime);
            }
            //Vector3 startPosition = Camera.main.WorldToScreenPoint(spawnObject.position);
            //Vector3 endPos = Input.mousePosition;

            //lineRenderer.SetPosition(0, imgProjection.position);
            //lineRenderer.SetPosition(1, Input.mousePosition);

            distance = (mouseWorldPos - transform.position).magnitude;

            customlineRenderer.SetPointPosition(0, imgProjection.position);
            customlineRenderer.SetPointPosition(1, Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            //Debug.Log("distance: " + distance);
            var rot = transform.rotation.eulerAngles;
            //Debug.Log("finalrot: " + rot);
            if (currentProjectile is not null && isDrag )
            {
                currentProjectile.gameObject.SetActive(true);
                dummyKnifeProjectile.gameObject.SetActive(false);
                Rigidbody projectileRigidBody = currentProjectile.GetComponent<Rigidbody>();
                projectileRigidBody.isKinematic = false;
                currentProjectile.rotation = transform.rotation;

                float forceAmount = 100;

                if (currentProjectile.tag == "Knife")
                    forceAmount = 30;
                else if (currentProjectile.tag == "Bomb")
                    forceAmount = 100;
                else
                    forceAmount = 80;

                projectileRigidBody.AddRelativeForce(-transform.up * forceAmount * distance, ForceMode.Impulse);

                if (currentProjectile.tag == "Bomb")
                {
                    Bomb bomb = currentProjectile.GetComponent<Bomb>();
                    bomb.explosion.SetActive(true);
                }
            }
            isDrag = false;
            //imgProjection.gameObject.SetActive(false);
            ResetLine();
        }
    }

    void ResetLine()
    {
        //lineRenderer.SetPosition(0, Vector3.zero);
        //lineRenderer.SetPosition(1, Vector3.zero);
        customlineRenderer.gameObject.SetActive(false);
    }

    public void RemoveAllProjectiles()
    {
        for (int i = 0; i < allProjectiles.Count; i++)
        {
            Destroy(allProjectiles[i]);
        }

        allProjectiles.Clear();
    }
}
