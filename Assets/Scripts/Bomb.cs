using RayFire;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public RayfireBomb rayfireBomb;
    public GameObject explosion;

    public void Blast()
    {
        rayfireBomb.Explode(0);
        if (explosion.activeSelf)
        {
            explosion.GetComponent<Detonator>().Explode();
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<SphereCollider>().enabled = false;
            //Destroy(gameObject.GetComponent<Rigidbody>());
            Destroy(gameObject, 5.0f);
        }
    }
}
