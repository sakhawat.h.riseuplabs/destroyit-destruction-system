using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDraw : MonoBehaviour
{
    public Camera mainCamera;
    public LineRenderer lineRenderer;
    public Transform startPos;
    public Transform centerPos;
    public Transform endPos;

    Vector3 initpos;
    bool isDrag = false;

    public Transform Projectile;
    private Transform currentProjectile;

    private void Start()
    {
        initpos = centerPos.position;
    }

    private void Update()
    {
        lineRenderer.SetPosition(0, startPos.position);
        lineRenderer.SetPosition(1, centerPos.position);
        lineRenderer.SetPosition(2, endPos.position);

        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                isDrag = true;
                currentProjectile = Instantiate(Projectile, centerPos.position, Quaternion.identity, centerPos);
            }

            if (!isDrag)
                return;

            Vector3 newMousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z);
            Vector3 newWorldPosition = Camera.main.ScreenToWorldPoint(newMousePosition);
            centerPos.position = newWorldPosition;

            Vector3 direction = newWorldPosition - initpos;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            currentProjectile.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

            lineRenderer.SetPosition(1, centerPos.position);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDrag = false;
            Rigidbody projectileRigidBody = currentProjectile.GetComponent<Rigidbody>();
            projectileRigidBody.isKinematic = false;
            projectileRigidBody.AddRelativeForce(-transform.up * 50, ForceMode.Impulse);
        }
        else
        {
            lineRenderer.SetPosition(1, initpos);
        }
    }
}