using System;
using RayFire;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class Test : MonoBehaviour
{
    public RayfireShatter rayfireShatter;

    public RayfireRigid rayfireRigid;

    public AimAndShoot aimAndShoot;

    public void Start()
    {
        RayfireRigid _rayfireRigid = Instantiate(rayfireRigid);
        _rayfireRigid.gameObject.SetActive(false);
        rayfireShatter.DeleteFragmentsLast();
        rayfireShatter.DeleteFragmentsAll();
        RayfireMan rayfireMan = FindObjectOfType<RayfireMan>();
        if(rayfireMan)
        {
            rayfireMan.DestroyStorage();
            rayfireMan.storage.DestroyAll();
            Transform tempTransform = rayfireMan.transform.GetChild(2);
            if(tempTransform.childCount > 0)
                foreach (Transform child in tempTransform)
                    Destroy(child.gameObject);
        }
        rayfireShatter.voronoi.amount = Random.Range(300, 1000);
        rayfireShatter.voronoi.centerBias = Random.Range(0f, 0.5f);
        rayfireShatter.Fragment();
        for (int i = 0; i < rayfireShatter.fragmentsLast.Count; i++)
        {
            rayfireShatter.fragmentsLast[i].transform.SetParent(_rayfireRigid.transform);
        }
        Destroy(rayfireShatter.rootChildList[^1].gameObject);
        rayfireShatter.rootChildList.RemoveAt (rayfireShatter.rootChildList.Count - 1);
        rayfireShatter.rootChildList.Add(_rayfireRigid.transform);
        rayfireShatter.meshRenderer.enabled = false;
        _rayfireRigid.gameObject.SetActive(true);
        // _rayfireRigid.initialized = true;

        aimAndShoot.RemoveAllProjectiles();
    }
}
